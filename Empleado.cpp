#include "Empleado.h"
#include "Fecha.h"
#include <iostream>
using namespace std;
Empleado::Empleado()
{
    nombreEmp="sinnombre";
    sueldoEmp=0;
    categoriaEmp=1;
    cumpleAņosEmp;
}

Empleado::~Empleado()
{
    
}

string Empleado::getNombreEmp()
{
    return nombreEmp;
}

float Empleado::getSueldoEmp()
{
    return sueldoEmp;
}

int Empleado::getCategoriaEmp()
{
    return categoriaEmp;
}

Fecha Empleado::getFecha()
{
    return cumpleAņosEmp;
}

void Empleado::setNombreEmp(string nom)
{
    nombreEmp = nom;

}

void Empleado::setCategoriaEmp(int cat)
{
    if (cat == 1 || cat == 2 || cat == 3 || cat == 4) {
        categoriaEmp = cat;
    }
    else {
       cout << "categoria ivalida";
    }
}

void Empleado::setSueldo(float dinero)
{
    sueldoEmp = dinero;
}

void Empleado::setFecha(Fecha date)
{
    cumpleAņosEmp = date;
}

void Empleado::promoverEmpleado(int cate)
{

    float dife = cate - categoriaEmp;
    if (dife >= 1) {
        sueldoEmp = sueldoEmp + (sueldoEmp * (dife * .025));
    }
    setCategoriaEmp(cate);


}
