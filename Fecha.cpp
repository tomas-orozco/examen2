using namespace std;
#include "Fecha.h"
#include "Empleado.h"

Fecha::Fecha()
{
	dia = 0;
	mes = 0;
}

Fecha::~Fecha()
{
}

int Fecha::getDia()
{
	return dia;
}

int Fecha::getMes()
{
	return mes;
}

void Fecha::setDia(int dia1)
{
	dia = dia1;

}

void Fecha::setMes(int mes1)
{
	mes = mes1;
}

